FROM olproject/onixos

RUN pacman -Sy --noconfirm yay rsync go xmlto python-sphinx base-devel go gnome-shell git qt qtcreator gperf ninja nasm openssh openssl sudo python nodejs node-gyp archiso
RUN echo "opackages ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
RUN groupadd opackages
RUN useradd -ms /bin/bash opackages -g opackages
RUN chown -Rv opackages:opackages /home/opackages
USER opackages
COPY --chown=opackages . /home/opackages/packages
WORKDIR /home/opackages/packages
ENV PATH=$PATH:/usr/bin:/usr/sbin:/bin:/home/opackages/go/bin
RUN go get -v github.com/seletskiy/go-makepkg
VOLUME ["/home/opackages"]
